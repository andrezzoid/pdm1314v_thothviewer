# Thoth Viewer for Android #

### Quick Summary ###
Thoth Viewer is an Android client for Thoth (http://thoth.cc.e.ipl.pt/), a web app for teachers and students to manage their academic life: courses, work items, news, groups, etc. Thoth Viewer allows students to register in a set of course units from a specific semester, see couse unit web pages and workitems, register calendar entries for workitem due dates and get notifications of new workitems and news.


### Technical Overview ###

Although Views were made simple, the course unit contents cover Android design with fragments. The app is asynchronous all the way - it uses Loaders, ContentProviders, Services, BroadcastReceivers - basicaly, some of the major building blocks of Android. Some code may come from previous iterations, when we had less Android knowledge :)


### Version ###
Thoth Viewer is made in **ISEL** (Instituto Superior de Engenharia de Lisboa) under the **PDM** (Mobile Devices Programming) course unit, 2013/2014 semester, taught by **Eng. Pedro Félix** and **Eng. João Trindade**. It's currently in version `0.2.0` and it's not actively maintained out of the course unit limits. We hope development can continue once we have more time in our hands :)

### Who do I talk to? ###

* Repo owner and admins
    + [andrezzoid](https://bitbucket.org/andrezzoid)
    + [araujo_ricardojorge](https://bitbucket.org/araujo_ricardojorge)


### Screenshots ###

![device-2014-06-13-mainactivity-wframe.png](https://bitbucket.org/repo/7g59og/images/3838003087-device-2014-06-13-mainactivity-wframe.png =100x100)
![device-2014-06-13-notifications-wframe.png](https://bitbucket.org/repo/7g59og/images/2091856300-device-2014-06-13-notifications-wframe.png =100x)
![device-2014-06-13-workitemsactivity-wframe.png](https://bitbucket.org/repo/7g59og/images/991154296-device-2014-06-13-workitemsactivity-wframe.png =200x)
![device-2014-06-13-workitemactivity-wframe.png](https://bitbucket.org/repo/7g59og/images/121701537-device-2014-06-13-workitemactivity-wframe.png =200x)