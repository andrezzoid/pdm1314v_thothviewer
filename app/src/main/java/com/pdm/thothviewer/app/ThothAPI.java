package com.pdm.thothviewer.app;

import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by André Jonas on 17-03-2014.
 */
public class ThothAPI {

    public static final String BASE_URL = "http://thoth.cc.e.ipl.pt/api/v1";

    public static class GET {

        public static class LectiveSemesters {
            public static final String URL = BASE_URL + "/lectivesemesters";
        }

        public static class Classes {
            public static final String URL = BASE_URL + "/classes";
            // página principal da disciplina
            // padrão: /classes/[COURSE_UNIT_SHORT_NAME]/[SEMESTER_SHORT_NAME]/[CLASS_NAME]
            public static final String MAIN_PAGE = "http://thoth.cc.e.ipl.pt/classes/%s/%s/%s/info";

            public static final String CLASSES = "classes";

            public static final String ID = "id";
            public static final String FULL_NAME = "fullName";
            public static final String COURSE_UNIT_SHORT_NAME = "courseUnitShortName";
            public static final String SEMESTER_SHORT_NAME = "lectiveSemesterShortName";
            public static final String CLASS_NAME = "className";
            public static final String MAIN_TEACHER_NAME = "mainTeacherShortName";
            public static final String LINKS = "_links";
            public static final String SELF = "self";
        }

        public static class WorkItems {
            public static final String URL = BASE_URL + "/classes/%d/workitems";

            public static final String WORKITEMS = "workItems";

            public static final String ID = "id";
            public static final String ACRONYM = "acronym";
            public static final String TITLE = "title";
            public static final String REQUIRES_SUBMISSION = "requiresGroupSubmission";
            public static final String START_DATE = "startDate";
            public static final String DUE_DATE = "dueDate";
            public static final String LINKS = "_links";
            public static final String SELF = "self";
        }
    }

    public static class PREFS{
        public static final String NAME = "thothviewerdetails";
        public static final String PREF_CURRENT_SEMESTER_STRING = "currentSemester";
        public static final String PREF_CLASSES_SET = "classes";
        public static final String PREFERENCES_SET = "preferencesesSet";
    }

}
