package com.pdm.thothviewer.app.models;

import android.util.Log;

import com.pdm.thothviewer.app.IParser;
import com.pdm.thothviewer.app.ThothAPI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by André Jonas on 27-03-2014.
 */
public class ClazzParser implements IParser<List<Clazz>> {

    private static final String TAG = ClazzParser.class.getSimpleName();

    private String semesterFilter = null;

    public ClazzParser(){}

    public ClazzParser(String semesterFilter) {
        this.semesterFilter = semesterFilter;
    }

    @Override
    public List<Clazz> parse(String json) throws ParseException {
        try {
            JSONObject jsonRoot = new JSONObject(json);
            JSONArray jsonClasses = jsonRoot.getJSONArray(ThothAPI.GET.Classes.CLASSES);
            List<Clazz> classes = new LinkedList();
            for (int i = 0; i < jsonClasses.length(); i++) {
                JSONObject clazz = jsonClasses.getJSONObject(i);
                String semesterName = clazz.getString(ThothAPI.GET.Classes.SEMESTER_SHORT_NAME);

                boolean isChosen = semesterFilter == null ||
                        (semesterFilter != null && semesterName.equals(semesterFilter));

                if (isChosen) {
                    classes.add(new Clazz(
                            clazz.getLong(ThothAPI.GET.Classes.ID),
                            clazz.getString(ThothAPI.GET.Classes.FULL_NAME),
                            clazz.getString(ThothAPI.GET.Classes.COURSE_UNIT_SHORT_NAME),
                            clazz.getString(ThothAPI.GET.Classes.SEMESTER_SHORT_NAME),
                            clazz.getString(ThothAPI.GET.Classes.CLASS_NAME)
                    ));
                }
            }
            return classes;
        }catch(JSONException e){
            Log.e(TAG, "JSONException" + e.getMessage());
            return null;
        }
    }
}
