package com.pdm.thothviewer.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.pdm.thothviewer.app.R;
import com.pdm.thothviewer.app.ThothAPI;
import com.pdm.thothviewer.app.utils.NetworkUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;

/**
 * Created by André Jonas on 24-03-2014.
 */
public class SemesterSettingsActivity extends Activity {

    private static final String TAG = "SemesterSettingsActivity";

    // dummy semesters
    // TODO: Load semesters from thoth
    private final String[] semesters = new String[]{
            "1314v",
            "1314i",
            "1213v",
            "1213i",
            "1112v",
            "1112i"
    };

    private ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_semester);

        // verificar se existe ligação à net
        if(!NetworkUtils.isOnline(this)){
            Toast.makeText(this, R.string.network_not_available, Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Network not available!");
            // termina explícitamente a Activity e retorna do método onCreate
            finish();
            return;
        }

        // inicialização da ListView
        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(
                new ArrayAdapter<String>(
                        this,
                        android.R.layout.simple_list_item_single_choice,
                        semesters
        ));

        // fazer set ao semestre previamente escolhido
        SharedPreferences prefs = getSharedPreferences(ThothAPI.PREFS.NAME, MODE_PRIVATE);
        String prefSemester = prefs.getString(ThothAPI.PREFS.PREF_CURRENT_SEMESTER_STRING, null);
        if(prefSemester != null){
            for(int i = 0; i < semesters.length; i++){
                if(prefSemester.equals(semesters[i])){
                    listView.setItemChecked(i, true);
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.confirm, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_confirm) {

            // sabe-se que é uma lista de Strings
            int position = listView.getCheckedItemPosition();
            String semesterName = (String)listView.getItemAtPosition(position);
            if(semesterName != null){
                SharedPreferences prefs = getSharedPreferences(ThothAPI.PREFS.NAME, MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(ThothAPI.PREFS.PREF_CURRENT_SEMESTER_STRING, semesterName);
                editor.apply(); //apply asynchronously to the file system.
                //editor.commit(); //The usage of the commit() method is discouraged, as it write the changes synchronously to the file system.
                // semestre escolhido e adicionado às preferêcias,
                // termina explícitamente a Activity e envia a informação para a actividade chamadora
                finish();
            }else{
                // utilizador não escolheu nenhum semestre e decidiu armar-se em macaco
                Toast.makeText(this, R.string.choose_one, Toast.LENGTH_LONG).show();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
