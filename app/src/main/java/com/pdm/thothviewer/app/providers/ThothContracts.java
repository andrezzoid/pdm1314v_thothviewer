package com.pdm.thothviewer.app.providers;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by André Jonas on 14-04-2014.
 *
 * MODOS DE OPERAÇÃO:
 *      + As inserções podem ser feitas num ID específico que pode ser passado no URI ou nos
 *      ContentValues;
 *      + As operações feitas em Batch notificam a raiz do Provider, ou seja, todos os Observers
 *      são notificados;
 *      + As inserções feitas em Batch que entrem em conflito com campos com restrições (ex: _ID
 *      chave primária), são inseridas com gestão de conflitos da seguinte forma: o registo já
 *      existente é removido e o novo é inserido, retornando o mesmo ou um novo identificador;
 *      + É possível obter todos os WorkItems de uma Class através do Uri 'classes/#/workitems'
 *      que pode ser construido com a função estática WorkItems.withClassId(long);
 *      + As consultas a WorkItems contêm também informação da Class a que pertencem. Internamente
 *      é feito um join entre ambas as tabelas;
 *
 * REQUERIMENTOS:
 *      + A inserção de um WorkItem deve ser feita através do modo especificado no 1º ponto da
 *      secção anterior e não utilizando o Uri 'classes/#/workitems', que lançará uma
 *      IllegalArgumentException neste caso. Opções de update ou delete também não deverão utilizar
 *      este Uri.
 *
 */
public final class ThothContracts {
    public static final String AUTHORITY = "com.pdm.thothviewer.provider";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    // Broadcast Intent para quando são recebidos novos workitems ou newsitems
    public static final String INTENT_ACTION_NEW_WORKITEMS = "com.pdm.thothviewer.intent.INTENT_ACTION_NEW_WORKITEMS";
    public static final String INTENT_ACTION_NEW_NEWSITEMS = "com.pdm.thothviewer.intent.INTENT_ACTION_NEW_NEWSITEMS";
    // Valores extra passados no Intent por broadcast
    public static final String EXTRA_CLASS_ID_LONG = "EXTRA_CLASS_ID";
    public static final String EXTRA_COURSE_NAME_STRING = "EXTRA_COURSE_NAME";
    public static final String EXTRA_COUNT_INT = "EXTRA_COUNT";

    public static final class Classes implements BaseColumns {
        // Nome da entidade e da tabela da BD
        public static final String TABLE_NAME = "classes";
        public static final Uri CONTENT_URI =
                Uri.withAppendedPath(ThothContracts.BASE_CONTENT_URI, TABLE_NAME);

        // Lista dos campos. Por motivos de simplicidade, são iguais aos da BD e da API,
        // à excepção do campo id, que no Provider e na BD começa com underscore (_id).
        public static final String FULL_NAME = "fullName";
        public static final String COURSE_UNIT_SHORT_NAME = "courseUnitShortName";
        public static final String SEMESTER_SHORT_NAME = "lectiveSemesterShortName";
        public static final String CLASS_NAME = "className";
        public static final String MAIN_TEACHER_NAME = "mainTeacherShortName";

        // projeção com todos os campos, útil para consultas ao Content Provider
        // podem-se adicionar aqui outras projeções úteis.
        public static final String[] PROJECTION_ALL = {
                _ID,
                FULL_NAME,
                COURSE_UNIT_SHORT_NAME,
                SEMESTER_SHORT_NAME,
                CLASS_NAME,
                MAIN_TEACHER_NAME
        };

        /**
         * Gera URI para Class específica
         * @param id
         * @return
         */
        public static Uri withId(long id){
            return ContentUris.withAppendedId(Classes.CONTENT_URI, id);
        }
    }

    public static final class WorkItems implements BaseColumns{
        // Nome da entidade e da tabela da BD
        public static final String TABLE_NAME = "workitems";
        public static final Uri CONTENT_URI =
                Uri.withAppendedPath(ThothContracts.BASE_CONTENT_URI, TABLE_NAME);

        // Lista dos campos. Por motivos de simplicidade, são iguais aos da BD e da API.
        // à excepção do campo id, que no Provider e na BD começa com underscore (_id).
        public static final String ACRONYM = "acronym";
        public static final String TITLE = "title";
        public static final String REQUIRES_SUBMISSION = "requiresGroupSubmission";
        public static final String START_DATE = "startDate";
        public static final String DUE_DATE = "dueDate";
        // não pertence à API mas é necessário para seleccionar os trabalhos de uma disciplina
        public static final String CLASS_ID = "classId";


        // projeção com todos os campos, útil para consultas ao Content Provider
        // podem-se adicionar aqui outras projeções úteis.
        public static final String[] PROJECTION_ALL = {
                _ID,
                ACRONYM,
                TITLE,
                REQUIRES_SUBMISSION,
                START_DATE,
                DUE_DATE,
                CLASS_ID,
                Classes.FULL_NAME,
                Classes.COURSE_UNIT_SHORT_NAME,
                Classes.SEMESTER_SHORT_NAME,
                Classes.CLASS_NAME,
                Classes.MAIN_TEACHER_NAME
        };

        public static final String[] PROJECTION_WORKITEMS_ONLY = {
                _ID,
                ACRONYM,
                TITLE,
                REQUIRES_SUBMISSION,
                START_DATE,
                DUE_DATE,
                CLASS_ID
        };

        /**
         *  Gera URI para WorkItem específico
         * @param id
         * @return
         */
        public static Uri withId(long id){
            return ContentUris.withAppendedId(WorkItems.CONTENT_URI, id);
        }

        /**
         * Gera um URI que permite obter todos os WorkItems de uma Class específica.
         * (Ex: '/classes/128/workitems')
         * @param classId
         * @return
         */
        public static Uri withClassId(long classId){
            return Uri.withAppendedPath(Classes.withId(classId), WorkItems.TABLE_NAME);
        }
    }

}
