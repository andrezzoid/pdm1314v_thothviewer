package com.pdm.thothviewer.app.receivers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.pdm.thothviewer.app.R;
import com.pdm.thothviewer.app.activities.WorkItemsActivity;
import com.pdm.thothviewer.app.providers.ThothContracts;

/**
 * Created by André Jonas on 12/05/2014.
 */
public class WorkItemNotificationReceiver extends BroadcastReceiver {
    private static final String TAG = WorkItemNotificationReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive()");

        long classId = intent.getLongExtra(ThothContracts.EXTRA_CLASS_ID_LONG, 0);
        String courseName = intent.getStringExtra(ThothContracts.EXTRA_COURSE_NAME_STRING);
        int newWorkitemsCount = intent.getIntExtra(ThothContracts.EXTRA_COUNT_INT, 0);

        Notification.Builder builder = new Notification.Builder(context)
                .setSmallIcon(R.drawable.ic_action_download)
                .setContentTitle(context.getString(R.string.new_workitems))
                .setContentText(courseName + " " +
                        context.getString(R.string.has_new_workitems))
                .setNumber(newWorkitemsCount)
                // Cancela automaticamente a notificação assim que o utilizador clica nela.
                .setAutoCancel(true);

        // Cria um intent explícito para uma Activity da aplicação
        // Quando o utilizador clica na notificação, será reencaminhado para a Activity.
        Intent resultIntent = new Intent(context, WorkItemsActivity.class);
        // Valor necessário à Activity
        resultIntent.putExtra(WorkItemsActivity.EXTRA_ID_LONG, classId);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                context,
                0,
                resultIntent,
                0
        );
        builder.setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify((int)classId, builder.getNotification());
    }
}
