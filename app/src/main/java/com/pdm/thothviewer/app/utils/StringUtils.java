package com.pdm.thothviewer.app.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by André Jonas on 19-03-2014.
 */
public class StringUtils {

    public static String getStringFrom(InputStream stream) throws IOException {
        StringBuilder builder = new StringBuilder();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(stream));
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            return builder.toString();
        }finally {
            if(reader != null)
                reader.close();
        }
    }
}
