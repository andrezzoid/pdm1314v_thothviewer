package com.pdm.thothviewer.app.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.pdm.thothviewer.app.ThothPrefs;

/**
 * Created by André Jonas on 13/05/2014.
 */
public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        ThothPrefs prefs = ThothPrefs.getInstance(context);
        // Como o update é feito com base no tempo desde o boot, quando é feito um novo boot, torna-se
        // necessário voltar com o marcador a zeros.
        prefs.setLastUpdate(0);
    }
}
