package com.pdm.thothviewer.app.models;

import com.pdm.thothviewer.app.R;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by André Jonas on 29-03-2014.
 */
public class WorkItem implements DomainModel {

    private static final SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");

    public long id;
    public String acronym;
    public String title;
    public boolean requiresSubmission;
    public long startDate;
    public long dueDate;

    public WorkItem(long id, String acronym, String title, boolean requiresSubmission, long startDate, long dueDate) {
        this.id = id;
        this.acronym = acronym;
        this.title = title;
        this.requiresSubmission = requiresSubmission;
        this.startDate = startDate;
        this.dueDate = dueDate;
    }

    public long getId() {
        return id;
    }

    public String getAcronym() {
        return acronym;
    }

    public String getTitle() {
        return title;
    }

    public boolean getRequiresSubmission() {
        return requiresSubmission;
    }

    public long getStartDateInMillis() {
        return startDate;
    }

    public long getDueDateInMillis() {
        return dueDate;
    }

    public String getStartDate() {
        return df.format(new Date(startDate));
    }

    public String getDueDate() {
        return df.format(new Date(dueDate));
    }
}
