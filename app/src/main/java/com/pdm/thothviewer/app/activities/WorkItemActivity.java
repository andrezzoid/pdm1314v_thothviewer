package com.pdm.thothviewer.app.activities;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.pdm.thothviewer.app.R;
import com.pdm.thothviewer.app.providers.ThothContracts;

/**
 * Created by André Jonas on 31-03-2014.
 */
public class WorkItemActivity extends Activity
    implements LoaderManager.LoaderCallbacks<Cursor>{

    private static final String TAG = WorkItemActivity.class.getSimpleName();

    public static final String EXTRA_ID_STRING = "id";
    public static final String EXTRA_ACRONYM_STRING = "acronym";
    public static final String EXTRA_TITLE_STRING = "title";
    public static final String EXTRA_SUBMISSION_BOOL = "requiresSubmission";
    public static final String EXTRA_START_DATE_LONG = "startDate";
    public static final String EXTRA_DUE_DATE_LONG = "dueDate";
    private static final int LOADER_ID = 1;


    private TextView txtTitle;
    private TextView txtSubmission;
    private TextView txtDueDate;

    private String courseName;
    private String title;
    private String submission;
    private long dueDate;
    /*
    WorkItem workItem;
    private String submissionString;
    */
    private long workItemId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workitem);

        // referências
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtSubmission = (TextView) findViewById(R.id.txtSubmission);
        txtDueDate = (TextView) findViewById(R.id.txtDueDate);

        // Obtem valores provenientes da lista de WorkItems
        // que continha o objecto WorkItem preenchido com a info necessária
        Intent intent = getIntent();
        /*
        long id = intent.getLongExtra(EXTRA_ID_STRING, 0);
        String acronym = intent.getStringExtra(EXTRA_ACRONYM_STRING);
        String title = intent.getStringExtra(EXTRA_TITLE_STRING);
        boolean reqSubmission = intent.getBooleanExtra(EXTRA_SUBMISSION_BOOL, false);
        long startDate = intent.getLongExtra(EXTRA_START_DATE_LONG, 0);
        long dueDate = intent.getLongExtra(EXTRA_DUE_DATE_LONG, 0);
        workItem = new WorkItem(id, acronym, title, reqSubmission, startDate, dueDate);
        // Título da Activity com o acrónimo do workitem
        setTitle(acronym);

        // preencher View
        txtTitle.setText(workItem.getTitle());
        submissionString = reqSubmission ? getString(R.string.with_submission) :
                getString(R.string.no_submission);
        txtSubmission.setText(submissionString);
        txtDueDate.setText(DateUtils.getRelativeTimeSpanString(this,
                workItem.getDueDateInMillis(),
                true)
        );
        */

        workItemId = intent.getLongExtra(EXTRA_ID_STRING, -1);
        if(workItemId == -1){
            Log.e(TAG, "Need a valid Class ID");
            finish();
            return;
        }
        getLoaderManager().initLoader(LOADER_ID, null, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.event, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_event) {
            Intent intent = new Intent(Intent.ACTION_INSERT)
                    .setData(CalendarContract.Events.CONTENT_URI)
                    .putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, true)
                    .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, dueDate)
                    .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, dueDate)
                    .putExtra(CalendarContract.Events.TITLE,
                            String.format("%s: %s", courseName, title));
            startActivity(intent);

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Log.d(TAG, "onCreateLoader()");
        return new CursorLoader(this,
                ThothContracts.WorkItems.withId(workItemId),
                ThothContracts.WorkItems.PROJECTION_ALL,
                null,
                null,
                null
                );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.d(TAG, "onLoadFinished()");
        if(data != null && data.getCount() > 0){
            // Extração de valores do Cursor
            data.moveToFirst();
            int acronymIdx = data.getColumnIndex(ThothContracts.WorkItems.ACRONYM);
            int titleIdx = data.getColumnIndex(ThothContracts.WorkItems.TITLE);
            int submissionIdx = data.getColumnIndex(ThothContracts.WorkItems.REQUIRES_SUBMISSION);
            int dueDateIdx = data.getColumnIndex(ThothContracts.WorkItems.DUE_DATE);
            int courseNameIdx = data.getColumnIndex(ThothContracts.Classes.COURSE_UNIT_SHORT_NAME);
            title = data.getString(titleIdx);
            int reqSubmission = data.getInt(submissionIdx);
            submission = reqSubmission == 1 ?
                    getString(R.string.with_submission) :
                    getString(R.string.no_submission);
            dueDate = data.getLong(dueDateIdx);
            courseName = data.getString(courseNameIdx);
            // Título da Activity
            setTitle(data.getString(acronymIdx));
            // Preencher a Activity com os valores
            txtTitle.setText(title);
            txtSubmission.setText(submission);
            txtDueDate.setText(DateUtils.getRelativeTimeSpanString(
                    this,
                    dueDate,
                    true)
            );
        }else{
            Toast.makeText(this, R.string.something_went_wrong, Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        Log.d(TAG, "onLoaderReset()");
        // Do nothing
    }
}
