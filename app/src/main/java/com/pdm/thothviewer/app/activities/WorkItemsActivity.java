package com.pdm.thothviewer.app.activities;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.pdm.thothviewer.app.HttpLoader;
import com.pdm.thothviewer.app.R;
import com.pdm.thothviewer.app.ThothAPI;
import com.pdm.thothviewer.app.adapters.AbstractArrayAdapter;
import com.pdm.thothviewer.app.models.WorkItem;
import com.pdm.thothviewer.app.models.WorkItemParser;
import com.pdm.thothviewer.app.models.WorkItemViewBinder;
import com.pdm.thothviewer.app.providers.ThothContracts;
import com.pdm.thothviewer.app.utils.NetworkUtils;

import java.util.List;

/**
 * Created by André Jonas on 30-03-2014.
 */
public class WorkItemsActivity extends Activity
        //implements LoaderManager.LoaderCallbacks<List<WorkItem>>,
        implements LoaderManager.LoaderCallbacks<Cursor>,
        AdapterView.OnItemClickListener{

    private static final String TAG = "WorkItemsActivity";
    private static final int LOADER_ID = 1;
    public static final String EXTRA_ID_LONG = "clazzId";
    public static final String EXTRA_NAME_STRING = "clazzName";
    public static final String EXTRA_SEMESTER_STRING = "semester";
    public static final String EXTRA_COURSE_STRING = "course";

    private ListView listView;
    private ProgressBar progress;

    private long clazzId;
    private String clazzName;
    private String courseName;
    private String semesterName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workitems);

        progress = (ProgressBar) findViewById(R.id.progressBar);
        listView = (ListView) findViewById(R.id.listView);
        // O Cursor não está pronto na criação do Adapter, passa-se null para o argumento do Cursor
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(
                this,
                R.layout.list_item_default,
                null,   // ainda não existe um Cursor populado com dados
                new String[]{
                        ThothContracts.WorkItems.ACRONYM,
                        ThothContracts.WorkItems.REQUIRES_SUBMISSION,
                        ThothContracts.WorkItems.DUE_DATE
                },
                new int[]{
                        R.id.txtMain,
                        R.id.txtSec,
                        R.id.txtSide
                },
                0
        );
        // ViewBinder que trata de mapear a row corretamente.
        adapter.setViewBinder(new WorkItemViewBinder(this));
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

        /*
        //
        // VERSÃO 0.1.0

        // verificar se existe ligação à net
        if(!NetworkUtils.isOnline(this)){
            Toast.makeText(this, R.string.network_not_available, Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Network not available!");
            // termina explícitamente a Activity e retorna do método onCreate
            finish();
            return;
        }
        */

        // obter dados necessários do Intent
        Intent intent = getIntent();
        clazzId = intent.getLongExtra(EXTRA_ID_LONG, -1);
        /*
        //
        // VERSÃO 0.1.0

        clazzName = intent.getStringExtra(EXTRA_NAME_STRING);
        courseName = intent.getStringExtra(EXTRA_COURSE_STRING);
        semesterName = intent.getStringExtra(EXTRA_SEMESTER_STRING);
        if(clazzId == -1 || clazzName == null){
            Log.d(TAG, "Need a valid Clazz id and name");
            finish();
            return;
        }


        // definir titulo da Activity com o nome da disciplina escolhida
        setTitle(clazzName);

        // inicializar o Loader para obter os WorkItems
        Bundle args = new Bundle();
        args.putLong(EXTRA_ID_LONG, clazzId);
        args.putString(EXTRA_NAME_STRING, clazzName);
        getLoaderManager().initLoader(LOADER_ID, args, this);
        */

        if(clazzId == -1){
            Log.e(TAG, "Need a valid Class ID");
            finish();
            return;
        }
        getLoaderManager().initLoader(LOADER_ID, null, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.homepage, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_homepage) {
            String url = String.format(
                    ThothAPI.GET.Classes.MAIN_PAGE,
                    courseName,
                    semesterName,
                    clazzName);
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    /*============================================================================================
     * LoaderManager.LoaderCallbacks
     =============================================================================================*/

    /*
    //
    // VERSÃO 0.1.0

    @Override
    public Loader<List<WorkItem>> onCreateLoader(int id, Bundle args) {
        Log.d(TAG, "onCreateLoader()");

        // Processamento prolongado vai começar, mostrar ProgressBar
        progress.setVisibility(View.VISIBLE);
        listView.setVisibility(View.GONE);

        String url = String.format(ThothAPI.GET.WorkItems.URL, clazzId);
        return new HttpLoader<List<WorkItem>>(this, url, new WorkItemParser());
    }

    @Override
    public void onLoadFinished(Loader<List<WorkItem>> loader, List<WorkItem> data) {
        Log.d(TAG, "onLoadFinished()");
        for(WorkItem w : data){
            Log.d(TAG, String.format("id: %d; title: %s;", w.getId(), w.getTitle()));
        }

        // Processamento terminou, esconder a ProgressBar
        progress.setVisibility(View.GONE);
        listView.setVisibility(View.VISIBLE);

        listView.setAdapter(new AbstractArrayAdapter<WorkItem>(
                this,
                R.layout.list_item_default,
                data
        ) {
            @Override
            public void bindRow(ViewHolder viewHolder, long position, WorkItem obj) {
                TextView main = viewHolder.get(R.id.txtMain);
                TextView sec = viewHolder.get(R.id.txtSec);
                TextView side = viewHolder.get(R.id.txtSide);
                side.setVisibility(View.GONE);
                main.setText(obj.getTitle());
                sec.setText(DateUtils.getRelativeTimeSpanString(getContext(), obj.getDueDateInMillis(), false));
            }
        });
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onLoaderReset(Loader<List<WorkItem>> loader) {
        Log.d(TAG, "onLoaderReset()");
        // do nothing
    }
    */

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Log.d(TAG, "onCreateLoader()");

        // Processamento prolongado vai começar, mostrar ProgressBar
        progress.setVisibility(View.VISIBLE);
        listView.setVisibility(View.GONE);

        return new CursorLoader(
                this,
                ThothContracts.WorkItems.withClassId(clazzId),  // só workitems da classe escolhida
                ThothContracts.WorkItems.PROJECTION_ALL,
                null,
                null,
                null
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.d(TAG, "onLoadFinished()");

        // Processamento terminou, esconder a ProgressBar
        progress.setVisibility(View.GONE);
        listView.setVisibility(View.VISIBLE);


        if(data != null && data.getCount() > 0){
            data.moveToFirst();

            // Necessário para a construção do URL, aquando o clique no options menu
            int classNameIdx = data.getColumnIndex(ThothContracts.Classes.CLASS_NAME);
            int courseNameIdx = data.getColumnIndex(ThothContracts.Classes.COURSE_UNIT_SHORT_NAME);
            int semesterNameIdx = data.getColumnIndex(ThothContracts.Classes.SEMESTER_SHORT_NAME);
            clazzName = data.getString(classNameIdx);
            courseName = data.getString(courseNameIdx);
            semesterName = data.getString(semesterNameIdx);

            // Título da Activity
            setTitle(courseName);

            ((SimpleCursorAdapter)listView.getAdapter()).swapCursor(data);
        }else{
            Toast.makeText(this, R.string.no_workitems_available, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        Log.d(TAG, "onLoaderReset()");
        // Processamento prolongado vai começar, mostrar ProgressBar
        progress.setVisibility(View.VISIBLE);
        listView.setVisibility(View.GONE);
        ((SimpleCursorAdapter)listView.getAdapter()).swapCursor(null);
    }


/*============================================================================================
     * AdapterView.OnItemClickListener
     =============================================================================================*/

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(this, WorkItemActivity.class);
        // É necessário o nome da Clazz logo, é necessário obter o elemento da ListView
        /*
        WorkItem item = (WorkItem)listView.getItemAtPosition(position);
        intent.putExtra(WorkItemActivity.EXTRA_ID_STRING, item.getId());
        intent.putExtra(WorkItemActivity.EXTRA_ACRONYM_STRING, item.getAcronym());
        intent.putExtra(WorkItemActivity.EXTRA_TITLE_STRING, item.getTitle());
        intent.putExtra(WorkItemActivity.EXTRA_SUBMISSION_BOOL, item.getRequiresSubmission());
        intent.putExtra(WorkItemActivity.EXTRA_START_DATE_LONG, item.getStartDateInMillis());
        intent.putExtra(WorkItemActivity.EXTRA_DUE_DATE_LONG, item.getDueDateInMillis());
        */
        intent.putExtra(WorkItemActivity.EXTRA_ID_STRING, id);
        startActivity(intent);
    }
}
