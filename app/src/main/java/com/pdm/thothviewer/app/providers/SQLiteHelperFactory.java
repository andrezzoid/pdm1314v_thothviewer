package com.pdm.thothviewer.app.providers;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by André Jonas on 06-12-2013.
 */
public class SQLiteHelperFactory {

    static final String TAG = "SQLiteHelperFactory";
    private static DbHelper dbHelper;

    public static synchronized SQLiteOpenHelper getInstance(Context context){
        if(dbHelper == null){
            dbHelper = new SQLiteHelperFactory.DbHelper(context);
            Log.d(TAG, "SQLiteOpenHelper instance created");
        }
        return dbHelper;
    }

    private static final class DbSchema{
        private static final String TBL_CLASS = ThothContracts.Classes.TABLE_NAME;

        private static final String CLASS_COL_ID = ThothContracts.Classes._ID;
        private static final String CLASS_COL_FULL_NAME = ThothContracts.Classes.FULL_NAME;
        private static final String CLASS_COL_COURSE_UNIT_SHORT_NAME = ThothContracts.Classes.COURSE_UNIT_SHORT_NAME;
        private static final String CLASS_COL_SEMESTER_SHORT_NAME = ThothContracts.Classes.SEMESTER_SHORT_NAME;
        private static final String CLASS_COL_CLASS_NAME = ThothContracts.Classes.CLASS_NAME;
        private static final String CLASS_COL_MAIN_TEACHER_NAME = ThothContracts.Classes.MAIN_TEACHER_NAME;


        private static final String TBL_WORKITEM = ThothContracts.WorkItems.TABLE_NAME;

        private static final String WORKITEM_COL_ID = ThothContracts.WorkItems._ID;
        private static final String WORKITEM_COL_ACRONYM = ThothContracts.WorkItems.ACRONYM;
        private static final String WORKITEM_COL_TITLE = ThothContracts.WorkItems.TITLE;
        private static final String WORKITEM_COL_REQUIRES_SUBMISSION = ThothContracts.WorkItems.REQUIRES_SUBMISSION;
        private static final String WORKITEM_COL_START_DATE = ThothContracts.WorkItems.START_DATE;
        private static final String WORKITEM_COL_DUE_DATE = ThothContracts.WorkItems.DUE_DATE;
        // não pertence à API mas é necessário para seleccionar os trabalhos de uma disciplina
        private static final String WORKITEM_COL_CLASS_ID = ThothContracts.WorkItems.CLASS_ID;


        //
        // CREATE TABLE SCRIPTS.
        // Por motivos de simplicidade, os nomes das tabelas são iguais aos nomes definidos pela
        // API e pelo Content Provider (aliás, são utilizadas as variáveis estáticas do Provider)
        //
        private static final String CREATE_TABLE_CLASS =
                "CREATE TABLE " + TBL_CLASS + "(" +
                        CLASS_COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        CLASS_COL_FULL_NAME + " TEXT NOT NULL," +
                        CLASS_COL_COURSE_UNIT_SHORT_NAME + " TEXT NOT NULL," +
                        CLASS_COL_SEMESTER_SHORT_NAME + " TEXT NOT NULL," +
                        CLASS_COL_CLASS_NAME + " TEXT NOT NULL," +
                        CLASS_COL_MAIN_TEACHER_NAME + " TEXT NOT NULL" +
                        ")";

        private static final String CREATE_TABLE_WORKITEM =
                "CREATE TABLE " + TBL_WORKITEM + "(" +
                        WORKITEM_COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        WORKITEM_COL_ACRONYM + " TEXT NOT NULL," +
                        WORKITEM_COL_TITLE + " TEXT NOT NULL," +
                        WORKITEM_COL_REQUIRES_SUBMISSION + " INTEGER NOT NULL," +
                        WORKITEM_COL_START_DATE + " INTEGER NOT NULL," +
                        WORKITEM_COL_DUE_DATE + " INTEGER NOT NULL," +
                        WORKITEM_COL_CLASS_ID + " INTEGER NOT NULL" +
                        ")";
    }

    private static class DbHelper extends SQLiteOpenHelper {

        private static final String TAG = "DbHelper";

        // If there's a change in the database schema, you must increment the database version.
        private static final int DATABASE_VERSION = 1;
        private static final String DATABASE_NAME = "thothviewer.db";
        // Name of the script to be parsed -> **IT MUST BE IN THE /assets folder!**
        private static final String DATABASE_CREATE = "thothviewer.sql";

        private Context mContext;

        public DbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            this.mContext = context;
        }

        /**
         * Executes an SQL file script in a given database. It basically loads all script to a
         * buffer, splits it by the ';' character and executes it one by one.
         * @param db
         * @param script
         */
        private void executeSQLScript(SQLiteDatabase db, String script) {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            byte buf[] = new byte[1024];
            int len;
            AssetManager assetManager = this.mContext.getAssets();
            InputStream inputStream = null;

            try{
                inputStream = assetManager.open(script);
                while ((len = inputStream.read(buf)) != -1) {
                    outputStream.write(buf, 0, len);
                }
                outputStream.close();
                inputStream.close();

                String[] createScript = outputStream.toString().split(";");
                for (int i = 0; i < createScript.length; i++) {
                    String sqlStatement = createScript[i].trim();
                    if (sqlStatement.length() > 0) {
                        Log.d("", sqlStatement);
                        db.execSQL(sqlStatement + ";");
                    }
                }
            } catch (IOException e){
                Log.e(TAG, e.getMessage());
            } catch (SQLException e) {
                Log.e(TAG, e.getMessage());
            }
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.d(TAG, "onCreate() Database");
            //this.executeSQLScript(db, DATABASE_CREATE);
            db.execSQL(DbSchema.CREATE_TABLE_CLASS);
            db.execSQL(DbSchema.CREATE_TABLE_WORKITEM);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.d(TAG, "onUpgrade() Database");
            this.onCreate(db);
            // do nothing
        }

        @Override
        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.d(TAG, "onDowngrade() Database");
            this.onUpgrade(db, oldVersion, newVersion);
            // do nothing
        }
    }
}
