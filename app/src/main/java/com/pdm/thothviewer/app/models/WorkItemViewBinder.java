package com.pdm.thothviewer.app.models;

import android.content.Context;
import android.database.Cursor;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.pdm.thothviewer.app.R;
import com.pdm.thothviewer.app.providers.ThothContracts;

/**
 * Created by André Jonas on 11/05/2014.
 */
public class WorkItemViewBinder implements SimpleCursorAdapter.ViewBinder {

    private final Context context;
    // Nomes das colunas do cursor, por ordem
    private String[] collumnNames;

    public WorkItemViewBinder(Context ctx){
        this.context = ctx;
    }

    @Override
    public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
        // boilerplate code
        if(collumnNames == null){
            // Só é feito uma vez, obtem a metadata das colunas para fazer mapeamento
            collumnNames = cursor.getColumnNames();
        }

        TextView txt = (TextView)view;

        if(collumnNames[columnIndex].equals(ThothContracts.WorkItems._ID) ||
                collumnNames[columnIndex].equals(ThothContracts.WorkItems.ACRONYM) ||
                collumnNames[columnIndex].equals(ThothContracts.WorkItems.TITLE)){

            txt.setText(cursor.getString(columnIndex));

        }else if(collumnNames[columnIndex].equals(ThothContracts.WorkItems.REQUIRES_SUBMISSION)){

            int reqSubmission = cursor.getInt(columnIndex);
            String submissionString = reqSubmission == 1 ?  // 1 == true
                    context.getString(R.string.with_submission) :
                    context.getString(R.string.no_submission);
            txt.setText(submissionString);

        }else if(collumnNames[columnIndex].equals(ThothContracts.WorkItems.START_DATE) ||
                collumnNames[columnIndex].equals(ThothContracts.WorkItems.DUE_DATE)){

            txt.setText(
                    DateUtils.getRelativeTimeSpanString(
                            context,
                            cursor.getLong(columnIndex),
                            true)
            );
        }
        return true;
    }
}
