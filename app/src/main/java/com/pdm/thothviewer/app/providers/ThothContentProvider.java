package com.pdm.thothviewer.app.providers;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

import com.pdm.thothviewer.app.utils.ThreadUtils;

import android.database.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by André Jonas on 14-04-2014.
 *
 * DISCLOSURE:
 * Implementação 'naive' do Content Provider, tendo em conta que é para consumo interno e será
 * bem utilizado no que diz respeito aos argumentos de projeção e seleção.
 */
public class ThothContentProvider extends ContentProvider {

    private static final String TAG = ThothContentProvider.class.getSimpleName();

    // URI ID's
    private static final int CLASSES_LIST = 1;
    private static final int CLASSES_ITEM = 2;
    private static final int WORKITEM_LIST = 3;
    private static final int WORKITEM_ITEM = 4;
    private static final int WORKITEM_LIST_BY_CLASS = 5;

    private static final Map<String, String> workItemsProjMap = new HashMap<String, String>();
    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static{
        //
        // Todas as Classes.
        // ex: /classes
        uriMatcher.addURI(
                ThothContracts.AUTHORITY,
                ThothContracts.Classes.TABLE_NAME,
                CLASSES_LIST);
        //
        // Class com ID específico.
        //  ex: /classes/128
        uriMatcher.addURI(
                ThothContracts.AUTHORITY,
                ThothContracts.Classes.TABLE_NAME + "/#",
                CLASSES_ITEM);
        //
        // Todos os WorkItems.
        //  ex: /workitems
        uriMatcher.addURI(
                ThothContracts.AUTHORITY,
                ThothContracts.WorkItems.TABLE_NAME,
                WORKITEM_LIST);
        //
        // WorkItem com ID específico.
        //  ex: /workitems/256
        uriMatcher.addURI(
                ThothContracts.AUTHORITY,
                ThothContracts.WorkItems.TABLE_NAME + "/#",
                WORKITEM_ITEM);
        //
        // Todos os WorkItems de uma Class específica.
        //  ex: /classes/128/workitems
        uriMatcher.addURI(
                ThothContracts.AUTHORITY,
                ThothContracts.Classes.TABLE_NAME + "/#/" + ThothContracts.WorkItems.TABLE_NAME,
                WORKITEM_LIST_BY_CLASS);

        //
        // Mapa de projeções
        // Tem o objetivo de desambiguar nomes (e.g. "acronym" -> "workitems.acronym")
        workItemsProjMap.put(ThothContracts.WorkItems._ID, ThothContracts.WorkItems.TABLE_NAME+"."+ThothContracts.WorkItems._ID);
        workItemsProjMap.put(ThothContracts.WorkItems.ACRONYM, ThothContracts.WorkItems.TABLE_NAME+"."+ThothContracts.WorkItems.ACRONYM);
        workItemsProjMap.put(ThothContracts.WorkItems.TITLE, ThothContracts.WorkItems.TABLE_NAME+"."+ThothContracts.WorkItems.TITLE);
        workItemsProjMap.put(ThothContracts.WorkItems.REQUIRES_SUBMISSION, ThothContracts.WorkItems.TABLE_NAME+"."+ThothContracts.WorkItems.REQUIRES_SUBMISSION);
        workItemsProjMap.put(ThothContracts.WorkItems.START_DATE, ThothContracts.WorkItems.TABLE_NAME+"."+ThothContracts.WorkItems.START_DATE);
        workItemsProjMap.put(ThothContracts.WorkItems.DUE_DATE, ThothContracts.WorkItems.TABLE_NAME+"."+ThothContracts.WorkItems.DUE_DATE);
        workItemsProjMap.put(ThothContracts.WorkItems.CLASS_ID, ThothContracts.WorkItems.TABLE_NAME+"."+ThothContracts.WorkItems.CLASS_ID);
        //workItemsProjMap.put(ThothContracts.Classes._ID, ThothContracts.Classes.TABLE_NAME+"."+ThothContracts.Classes._ID);
        workItemsProjMap.put(ThothContracts.Classes.FULL_NAME, ThothContracts.Classes.TABLE_NAME+"."+ThothContracts.Classes.FULL_NAME);
        workItemsProjMap.put(ThothContracts.Classes.COURSE_UNIT_SHORT_NAME, ThothContracts.Classes.TABLE_NAME+"."+ThothContracts.Classes.COURSE_UNIT_SHORT_NAME);
        workItemsProjMap.put(ThothContracts.Classes.SEMESTER_SHORT_NAME, ThothContracts.Classes.TABLE_NAME+"."+ThothContracts.Classes.SEMESTER_SHORT_NAME);
        workItemsProjMap.put(ThothContracts.Classes.CLASS_NAME, ThothContracts.Classes.TABLE_NAME+"."+ThothContracts.Classes.CLASS_NAME);
        workItemsProjMap.put(ThothContracts.Classes.MAIN_TEACHER_NAME, ThothContracts.Classes.TABLE_NAME+"."+ThothContracts.Classes.MAIN_TEACHER_NAME);

    }

    private SQLiteOpenHelper dbHelper;

    // Cada Thread que acede (via get ou set) tem a sua própria cópia da variável
    // Permite decidir a natureza das notificações e inserções, caso as operações sejam feitas em
    // Batch ou não (quando em Batch, deve-se notificar apenas no final).
    private final ThreadLocal<Boolean> inBatchMode = new ThreadLocal<Boolean>(){
        /**
         * Provides the initial value of this variable for the current thread.
         * The default implementation returns {@code null}.
         *
         * @return the initial value of the variable.
         */
        @Override
        protected Boolean initialValue() {
            return false;
        }
    };


    @Override
    public boolean onCreate() {
        Log.d(TAG, "onCreate()");
        ThreadUtils.logThreadInfo(TAG);
        dbHelper = SQLiteHelperFactory.getInstance(getContext());
        return true;
    }

    @Override
    public void shutdown() {
        Log.d(TAG, "shutdown()");
        ThreadUtils.logThreadInfo(TAG);
        // debug
        super.shutdown();
    }

    /**
     * Override this to handle requests to perform a batch of operations, or the
     * default implementation will iterate over the operations and call
     * {@link android.content.ContentProviderOperation#apply} on each of them.
     * If all calls to {@link android.content.ContentProviderOperation#apply} succeed
     * then a {@link android.content.ContentProviderResult} array with as many
     * elements as there were operations will be returned.  If any of the calls
     * fail, it is up to the implementation how many of the others take effect.
     * This method can be called from multiple threads, as described in
     * <a href="{@docRoot}guide/topics/fundamentals/processes-and-threads.html#Threads">Processes
     * and Threads</a>.
     *
     * @param operations the operations to apply
     * @return the results of the applications
     * @throws android.content.OperationApplicationException thrown if any operation fails.
     * @see android.content.ContentProviderOperation#apply
     */
    @Override
    public ContentProviderResult[] applyBatch(ArrayList<ContentProviderOperation> operations) throws OperationApplicationException {
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        //
        inBatchMode.set(true);
        // Garantia de transacção
        db.beginTransaction();
        try{
            ContentProviderResult[] results = super.applyBatch(operations);
            db.setTransactionSuccessful();
            // Como não sabemos quais os  URI's utilizados nas diversas operações, notificamos
            // o CONTENT_URI base do provider
            getContext().getContentResolver().notifyChange(ThothContracts.BASE_CONTENT_URI, null);
            return results;
        }finally {
            inBatchMode.set(false);
            db.endTransaction();
        }
    }


    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        //return super.bulkInsert(uri, values);
        // não queremos suportar esta operação, apenas para garantir.
        throw new UnsupportedOperationException();
    }


    /*
         * @param uri           The URI to query. This will be the full URI sent by the client;
         *                      if the client is requesting a specific record, the URI will end in a record number
         *                      that the implementation should parse and add to a WHERE or HAVING clause, specifying
         *                      that _id value.
         * @param projection    The list of columns to put into the cursor. If
         *                      {@code null} all columns are included.
         * @param selection     A selection criteria to apply when filtering rows.
         *                      If {@code null} then all rows are included.
         * @param selectionArgs You may include ?s in selection, which will be replaced by
         *                      the values from selectionArgs, in order that they appear in the selection.
         *                      The values will be bound as Strings.
         * @param sortOrder     How the rows in the cursor should be sorted.
         *                      If {@code null} then the provider is free to define the sort order.
         * @return a Cursor or {@code null}.
         */
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Log.d(TAG, "query()");
        ThreadUtils.logThreadInfo(TAG);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();

        switch (uriMatcher.match(uri)){
            case CLASSES_LIST:
                builder.setTables(ThothContracts.Classes.TABLE_NAME);
                break;

            case CLASSES_ITEM:
                builder.setTables(ThothContracts.Classes.TABLE_NAME);
                // obter o ID passsado pelo URI e adicionar à clausula WHERE
                long classId = ContentUris.parseId(uri);
                builder.appendWhere(String.format("%s=%d", ThothContracts.Classes._ID, classId));
                break;

            case WORKITEM_LIST_BY_CLASS:
                //builder.setTables(ThothContracts.WorkItems.TABLE_NAME);
                // Inner Join entre Class e Workitems
                builder.setProjectionMap(workItemsProjMap);
                builder.setTables(String.format("%s inner JOIN %s ON (%s.%s = %s.%s)",
                        ThothContracts.WorkItems.TABLE_NAME,
                        ThothContracts.Classes.TABLE_NAME,
                        ThothContracts.WorkItems.TABLE_NAME,
                        ThothContracts.WorkItems.CLASS_ID,
                        ThothContracts.Classes.TABLE_NAME,
                        ThothContracts.Classes._ID
                ));
                // Obtem o ID da classe pela sua posição no path do Uri
                long wClassId = Long.parseLong(uri.getPathSegments().get(1));
                builder.appendWhere(
                        String.format("%s=%d", ThothContracts.WorkItems.CLASS_ID, wClassId));
                break;

            case WORKITEM_LIST:
                //builder.setTables(ThothContracts.WorkItems.TABLE_NAME);
                // Inner Join entre Class e Workitems
                builder.setProjectionMap(workItemsProjMap);
                builder.setTables(String.format("%s LEFT OUTER JOIN %s ON (%s.%s = %s.%s)",
                        ThothContracts.WorkItems.TABLE_NAME,
                        ThothContracts.Classes.TABLE_NAME,
                        ThothContracts.WorkItems.TABLE_NAME,
                        ThothContracts.WorkItems.CLASS_ID,
                        ThothContracts.Classes.TABLE_NAME,
                        ThothContracts.Classes._ID
                ));
                break;

            case WORKITEM_ITEM:
                //builder.setTables(ThothContracts.WorkItems.TABLE_NAME);
                // Inner Join entre Class e Workitems
                builder.setProjectionMap(workItemsProjMap);
                builder.setTables(String.format("%s LEFT OUTER JOIN %s ON (%s.%s = %s.%s)",
                        ThothContracts.WorkItems.TABLE_NAME,
                        ThothContracts.Classes.TABLE_NAME,
                        ThothContracts.WorkItems.TABLE_NAME,
                        ThothContracts.WorkItems.CLASS_ID,
                        ThothContracts.Classes.TABLE_NAME,
                        ThothContracts.Classes._ID
                ));
                // obter o ID passsado pelo URI e adicionar à clausula WHERE
                long itemId = ContentUris.parseId(uri);
                builder.appendWhere(String.format("%s.%s=%d",
                        ThothContracts.WorkItems.TABLE_NAME,
                        ThothContracts.WorkItems._ID,
                        itemId));
                break;

            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
        try {
            Cursor c = builder.query(db,
                    projection,
                    null,   //selection,
                    null,   //selectionArgs,
                    null,
                    null,
                    sortOrder);

            // Regista o URI para obter notificação sobre modificações ao conteúdo
            c.setNotificationUri(getContext().getContentResolver(), uri);
            return c;
        }catch (Exception e){
            return null;
        }
    }


    @Override
    public String getType(Uri uri) {
        // don't care
        return null;
    }


    @Override
    public Uri insert(Uri uri, ContentValues values) {
        Log.d(TAG, "insert()");
        ThreadUtils.logThreadInfo(TAG);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Uri content_uri = null;
        String table = null;
        long newId = 0;

        switch (uriMatcher.match(uri)){
            //
            // Implementação corrente permite que o ID a inserir seja passado no URI, no
            // ContentValues recebido (campo '_id') ou não seja passado (é gerado pela BD)

            case CLASSES_ITEM:
                // Obtém ID do novo item a criar
                long classId = ContentUris.parseId(uri);
                if(values != null){
                    values.put(ThothContracts.Classes._ID, classId);
                }

            case CLASSES_LIST:
                table = ThothContracts.Classes.TABLE_NAME;
                content_uri = ThothContracts.Classes.CONTENT_URI;
                break;

            case WORKITEM_ITEM:
                // Obtém ID do novo item a criar
                newId = ContentUris.parseId(uri);
                if(values != null){
                    values.put(ThothContracts.Classes._ID, newId);
                }

            case WORKITEM_LIST:
                table = ThothContracts.WorkItems.TABLE_NAME;
                content_uri = ThothContracts.WorkItems.CONTENT_URI;
                break;

            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        if(inBatchMode.get()){
            // Se está em BatchMode, insere mesmo com conflito com o mesmo ID.
            // SQLiteDatabase.CONFLICT_REPLACE Remove o anterior e insere o novo.
            Log.d(TAG, "Inserting with conflict replace");
            newId = db.insertWithOnConflict(table, null, values, SQLiteDatabase.CONFLICT_REPLACE);
        }else{
            newId = db.insert(table, null, values);
        }

        if(newId > 0){
            // retorna URI para o novo item
            Uri newUri = ContentUris.withAppendedId(content_uri, newId);
            if(!inBatchMode.get()) {
                // notifica todos os registados no URI base
                // ninguém está registado num URI não existia até agora
                getContext().getContentResolver().notifyChange(content_uri, null);
            }
            return newUri;
        }else{
            throw new SQLException("Problem while inserting into uri: " + uri);
        }
    }


    /*
     * @param uri           The full URI to query, including a row ID (if a specific record is requested).
     * @param selection     An optional restriction to apply to rows when deleting.
     * @param selectionArgs
     * @return The number of rows affected.
     * @throws android.database.SQLException
     */
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        Log.d(TAG, "delete()");
        ThreadUtils.logThreadInfo(TAG);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int affected = 0;
        switch (uriMatcher.match(uri)){

            case CLASSES_ITEM:
                // obter o ID passsado pelo URI e adicionar à clausula WHERE
                long classId = ContentUris.parseId(uri);
                if(selection == null){
                    selection = String.format("%s=%d", ThothContracts.Classes._ID, classId);
                }else{
                    selection += String.format(" AND %s=%d", ThothContracts.Classes._ID, classId);
                }

            case CLASSES_LIST:
                affected = db.delete(
                        ThothContracts.Classes.TABLE_NAME,
                        selection,
                        selectionArgs
                );
                break;

            case WORKITEM_ITEM:
                // obter o ID passsado pelo URI e adicionar à clausula WHERE
                long itemId = ContentUris.parseId(uri);
                if(selection == null){
                    selection = String.format("%s=%d", ThothContracts.WorkItems._ID, itemId);
                }else{
                    selection += String.format(" AND %s=%d", ThothContracts.WorkItems._ID, itemId);
                }

            case WORKITEM_LIST:
                affected = db.delete(
                        ThothContracts.WorkItems.TABLE_NAME,
                        selection,
                        selectionArgs
                );
                break;

            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        if(affected > 0 && !inBatchMode.get()){
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return affected;
    }


    /*
     * @param uri           The URI to query. This can potentially have a record ID if this
     *                      is an update request for a specific record.
     * @param values        A set of column_name/value pairs to update in the database.
     *                      This must not be {@code null}.
     * @param selection     An optional filter to match rows to update.
     * @param selectionArgs
     * @return the number of rows affected.
     */
    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        Log.d(TAG, "update()");
        ThreadUtils.logThreadInfo(TAG);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int affected = 0;
        switch (uriMatcher.match(uri)){

            case CLASSES_ITEM:
                // obter o ID passsado pelo URI e adicionar à clausula WHERE
                long classId = ContentUris.parseId(uri);
                if(selection == null){
                    selection = String.format("%s=%d", ThothContracts.Classes._ID, classId);
                }else{
                    selection += String.format(" AND %s=%d", ThothContracts.Classes._ID, classId);
                }

            case CLASSES_LIST:
                affected = db.update(
                        ThothContracts.Classes.TABLE_NAME,
                        values,
                        selection,
                        selectionArgs
                );
                break;

            case WORKITEM_ITEM:
                // obter o ID passsado pelo URI e adicionar à clausula WHERE
                long itemId = ContentUris.parseId(uri);
                if(selection == null){
                    selection = String.format("%s=%d", ThothContracts.WorkItems._ID, itemId);
                }else{
                    selection += String.format(" AND %s=%d", ThothContracts.WorkItems._ID, itemId);
                }

            case WORKITEM_LIST:
                affected = db.update(
                        ThothContracts.WorkItems.TABLE_NAME,
                        values,
                        selection,
                        selectionArgs
                );
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        if(affected > 0 && !inBatchMode.get()){
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return affected;
    }
}
