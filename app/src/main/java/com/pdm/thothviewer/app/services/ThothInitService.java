package com.pdm.thothviewer.app.services;

import android.app.IntentService;
import android.content.ContentProviderOperation;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

import com.pdm.thothviewer.app.ThothAPI;
import com.pdm.thothviewer.app.ThothPrefs;
import com.pdm.thothviewer.app.providers.ThothContracts;
import com.pdm.thothviewer.app.utils.NetworkUtils;
import com.pdm.thothviewer.app.utils.StringUtils;
import com.pdm.thothviewer.app.utils.ThreadUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by André Jonas on 01-05-2014.
 */
public class ThothInitService extends IntentService {

    public static final String TAG = ThothInitService.class.getSimpleName();

    private static ContentProviderOperation jsonClassToOperation(JSONObject obj) throws JSONException {
        return ContentProviderOperation.newInsert(ThothContracts.Classes.CONTENT_URI)
                .withValue(ThothContracts.Classes._ID, obj.getLong(ThothAPI.GET.Classes.ID))
                .withValue(ThothContracts.Classes.FULL_NAME, obj.getString(ThothAPI.GET.Classes.FULL_NAME))
                .withValue(ThothContracts.Classes.COURSE_UNIT_SHORT_NAME, obj.getString(ThothAPI.GET.Classes.COURSE_UNIT_SHORT_NAME))
                .withValue(ThothContracts.Classes.SEMESTER_SHORT_NAME, obj.getString(ThothAPI.GET.Classes.SEMESTER_SHORT_NAME))
                .withValue(ThothContracts.Classes.CLASS_NAME, obj.getString(ThothAPI.GET.Classes.CLASS_NAME))
                .withValue(ThothContracts.Classes.MAIN_TEACHER_NAME, obj.getString(ThothAPI.GET.Classes.MAIN_TEACHER_NAME))
                        // The flip side of using batched operations is that a large batch may lock up
                        // the database for a long time preventing other applications from accessing
                        // data and potentially causing ANRs ("Application Not Responding" dialogs).
                        // To avoid such lockups of the database, make sure to insert "yield points" in
                        // the batch. A yield point indicates to the content provider that before
                        // executing the next operation it can commit the changes that have already
                        // been made, yield to other requests, open another transaction and continue
                        // processing operations. A yield point will not automatically commit the
                        // transaction, but only if there is another request waiting on the database.
                        // Normally a sync adapter should insert a yield point at the beginning of each
                        // raw contact operation sequence in the batch.
                .withYieldAllowed(true)
                .build();
    }


    public ThothInitService() {
        super(TAG + "WorkerThread");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG, "onHandleIntent()");
        ThreadUtils.logThreadInfo(TAG);
        
        ThothPrefs prefs = ThothPrefs.getInstance(this);
        final List<String> classesList = prefs.getClassesList();
        final String semester = prefs.getSemesterShortName();
        if(classesList != null && semester != null){
            try {
                // Remoção das Classes e Workitems anteriores (não interessa se existem classes
                // repetidas, vamos carregar tudo.
                getContentResolver().delete(
                        ThothContracts.Classes.CONTENT_URI,
                        null,
                        null
                );
                getContentResolver().delete(
                        ThothContracts.WorkItems.CONTENT_URI,
                        null,
                        null
                );

                // Pedido HTTP por todas as Classes
                String classJson = NetworkUtils.httpGet(ThothAPI.GET.Classes.URL);
                if(classJson != null) {
                    ArrayList<ContentProviderOperation> ops =
                            new ArrayList<ContentProviderOperation>();
                    // Obtém array do objeto raiz e percorre
                    JSONObject classesRoot = new JSONObject(classJson);
                    JSONArray jsonClasses =
                            classesRoot.getJSONArray(ThothAPI.GET.Classes.CLASSES);
                    for (int i = 0; i < jsonClasses.length(); i++) {
                        // Obtém cada uma das Disciplinas
                        JSONObject clazz = jsonClasses.getJSONObject(i);
                        String semesterName =
                                clazz.getString(ThothAPI.GET.Classes.SEMESTER_SHORT_NAME);
                        String clazzId = clazz.getString(ThothAPI.GET.Classes.ID);
                        // Condição para disciplina a seleccionar
                        boolean isChosen = semesterName.equals(semester) &&
                                classesList.contains(clazzId);
                        if (isChosen) {
                            // Adiciona às operações a aplicar mais à frente pelo Resolver
                            ops.add(jsonClassToOperation(clazz));
                        }
                    }

                    getContentResolver().applyBatch(ThothContracts.AUTHORITY, ops);

                    // O serviço corrente terminou a inserção de disciplinas, inicia explícitamente
                    // o serviço de update para actualizar workitems e newsitems
                    Intent updateIntent = new Intent(this, ThothUpdateService.class);
                    startService(updateIntent);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
