package com.pdm.thothviewer.app.activities;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import com.pdm.thothviewer.app.R;
import com.pdm.thothviewer.app.ThothAPI;
import com.pdm.thothviewer.app.models.Clazz;
import com.pdm.thothviewer.app.providers.ThothContracts;
import com.pdm.thothviewer.app.services.ThothUpdateService;
import com.pdm.thothviewer.app.utils.NetworkUtils;

import java.util.ArrayList;
import java.util.Set;


public class MainActivity extends Activity
        implements //LoaderManager.LoaderCallbacks<List<Clazz>>,
        LoaderManager.LoaderCallbacks<Cursor>,
        AdapterView.OnItemClickListener{

    private static final String TAG = "MainActivity";
    private static final int LOADER_ID = 0;
    private static final int PREF_SET = 2;
    private static int curr_pref = 0;
    public static final String EXTRA_ID_LONG = "clazzId";
    public static final String EXTRA_NAME_STRING = "clazzName";
    private static final String EXTRA_SEMESTER_STRING = "semester";
    private static final String EXTRA_CLAZZES_STRING = "clazzes";
    private static final String EXTRA_COURSE_STRING = "course";

    private ListView listView;
    private ProgressBar progress;
    private String semesterName;
    private ArrayList<String> clazzesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // obter referências para a View
        progress = (ProgressBar) findViewById(R.id.progressBar);
        listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(this);

        // O Cursor não está pronto na criação do Adapter, passa-se null para o argumento do Cursor
        listView.setAdapter(new SimpleCursorAdapter(
                this,
                R.layout.list_item_default,
                null,   // ainda não existe um Cursor populado com dados
                new String[]{
                        ThothContracts.Classes.COURSE_UNIT_SHORT_NAME,
                        ThothContracts.Classes.MAIN_TEACHER_NAME,
                        ThothContracts.Classes.CLASS_NAME
                },
                new int[]{
                        R.id.txtMain,
                        R.id.txtSec,
                        R.id.txtSide
                },
                0
        ));

        getLoaderManager().initLoader(LOADER_ID, null, this);

        /*

        //
        // CÓDIGO DEPRECATED DA VERSÃO 0.1.0
        //

        // verificar se existe ligação à net
        if(!NetworkUtils.isOnline(this)){
            Toast.makeText(this, R.string.network_not_available, Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Network not available!");
            // não continua o restante código do método onCreate
            return;
        }
        */
    }

    /**
     *  Set the user preferences for first run.
     *  If network is available the user preferences
     *  setPreferences() is called three times:
     *      1. Set Current Semester
     *      2. Set Classes
     *      3. Verify that 1. and 2. are set and set that all preferences are indeed set
     */
    public void onResume(){
        super.onResume();
        if(isNetworkAvailable()) {
            SharedPreferences prefs = getSharedPreferences(ThothAPI.PREFS.NAME, MODE_PRIVATE);
            String preferencesesSet = prefs.getString(ThothAPI.PREFS.PREFERENCES_SET, null);
            if (preferencesesSet == null) {
                setPreferences(prefs);
            }
        } else {
            Toast.makeText(this, "Sem Conexão", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     *  Set the user preferences for Current Semester
     *  and Classes.
     */
    private void setPreferences(SharedPreferences prefs){
        if (prefs != null) {
            String currentSemester = prefs.getString(ThothAPI.PREFS.PREF_CURRENT_SEMESTER_STRING, null);
            Set<String> classes = prefs.getStringSet(ThothAPI.PREFS.PREF_CLASSES_SET, null);

            if (currentSemester != null && classes != null) {
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString(ThothAPI.PREFS.PREFERENCES_SET, "Preferences are SET");
                editor.apply();
            }

            if (currentSemester == null) {
                startActivity(new Intent(this, SemesterSettingsActivity.class));
            }
            if (currentSemester != null && classes == null) {
                startActivity(new Intent(this, ClazzesSettingsActivity.class));
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if(id == R.id.action_refresh){
            if(NetworkUtils.isOnlineWiFi(this)) {
                //getLoaderManager().restartLoader(LOADER_ID, null, this);
                // Intent explícito para serviço de update
                // Não creio que seja necessário adquirir um wake lock porque não é uma tarefa
                // crítica que tem que ser completada "no matter what". Do ponto de vista prático,
                // o utilizador clicou explícitamente no botão refresh e ficará à espera do update.
                Intent intent = new Intent(this, ThothUpdateService.class);
                startService(intent);
                // Hint visual de que o update está "em processamento"
                progress.setVisibility(View.VISIBLE);
                listView.setVisibility(View.GONE);
            }
            return true;
        }else if (id == R.id.action_settings_semester) {
            startActivity(new Intent(this, SemesterSettingsActivity.class));
            return true;
        }else if(id == R.id.action_settings_classes){
            startActivity(new Intent(this, ClazzesSettingsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return (activeNetworkInfo != null) && activeNetworkInfo.isConnected();
    }

    /*============================================================================================
     * LoaderManager.LoaderCallbacks
     =============================================================================================*/
/*
    //
    // CÓDIGO DEPRECATED DA VERSÃO 0.1.0
    //

    @Override
    public Loader<List<Clazz>> onCreateLoader(int id, Bundle args) {
        Log.d(TAG, "onCreateLoader()");

        // obter valores das Shared Preferences
        SharedPreferences prefs = getSharedPreferences("thothviewerdetails", MODE_PRIVATE);
        semesterName = prefs.getStringFrom(ThothAPI.PREFS.PREF_CURRENT_SEMESTER_STRING, " ");
        clazzesList = new ArrayList<String>(
                prefs.getStringSet(ThothAPI.PREFS.PREF_CLASSES_SET, new HashSet<String>())
        );

        // Processamento prolongado vai começar, mostrar ProgressBar
        progress.setVisibility(View.VISIBLE);
        listView.setVisibility(View.GONE);

        return new HttpLoader<List<Clazz>>(
                this,
                ThothAPI.GET.Classes.URL,
                new ClazzFilterParser(semesterName, clazzesList)
        );
    }

    @Override
    public void onLoadFinished(Loader<List<Clazz>> loader, List<Clazz> data) {
        Log.d(TAG, "onLoadFinished()");

        // Processamento terminou, esconder a ProgressBar
        progress.setVisibility(View.GONE);
        listView.setVisibility(View.VISIBLE);

        if(data == null || data.isEmpty()){
            Log.d(TAG, "data is null");
            Toast.makeText(this, R.string.no_classes_available, Toast.LENGTH_LONG).show();
        }else{
            listView.setAdapter(new AbstractArrayAdapter<Clazz>(
                    this,
                    R.layout.list_item_default,
                    data
            ) {
                @Override
                public void bindRow(ViewHolder viewHolder, long position, Clazz obj) {
                    TextView main = viewHolder.get(R.id.txtMain);
                    TextView sec = viewHolder.get(R.id.txtSec);
                    TextView side = viewHolder.get(R.id.txtSide);
                    side.setVisibility(View.GONE);
                    main.setText(obj.getCourseShortName());
                    sec.setText(obj.getSemesterShortName() + " " + obj.getClassName());
                }
            });
            listView.setOnItemClickListener(this);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Clazz>> loader) {
        Log.d(TAG, "onLoaderReset()");
        // do nothing
    }
*/

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Log.d(TAG, "onCreateLoader()");

        // Processamento prolongado vai começar, mostrar ProgressBar
        progress.setVisibility(View.VISIBLE);
        listView.setVisibility(View.GONE);

        return new CursorLoader(
                this,
                ThothContracts.Classes.CONTENT_URI,
                ThothContracts.Classes.PROJECTION_ALL,
                null,
                null,
                null
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.d(TAG, "onLoadFinished()");

        // Processamento terminou, esconder a ProgressBar
        progress.setVisibility(View.GONE);
        listView.setVisibility(View.VISIBLE);

        if(data != null && data.getCount() > 0){
            data.moveToFirst();
            ((SimpleCursorAdapter)listView.getAdapter()).swapCursor(data);
        }else{
            Toast.makeText(this, R.string.no_classes_available, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        Log.d(TAG, "onLoaderReset()");
        // Processamento prolongado vai começar, mostrar ProgressBar
        progress.setVisibility(View.VISIBLE);
        listView.setVisibility(View.GONE);
        ((SimpleCursorAdapter)listView.getAdapter()).swapCursor(null);
    }

    /*============================================================================================
     * AdapterView.OnItemClickListener
     =============================================================================================*/

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(this, WorkItemsActivity.class);
        // É necessário o nome da Clazz logo, é necessário obter o elemento da ListView
        /*
        Clazz c = (Clazz)listView.getItemAtPosition(position);
        intent.putExtra(WorkItemsActivity.EXTRA_ID_LONG, c.getId());
        intent.putExtra(WorkItemsActivity.EXTRA_NAME_STRING, c.getClassName());
        intent.putExtra(WorkItemsActivity.EXTRA_SEMESTER_STRING, c.getSemesterShortName());
        intent.putExtra(WorkItemsActivity.EXTRA_COURSE_STRING, c.getCourseShortName());
        */
        intent.putExtra(WorkItemsActivity.EXTRA_ID_LONG, id);
        startActivity(intent);
    }
}
