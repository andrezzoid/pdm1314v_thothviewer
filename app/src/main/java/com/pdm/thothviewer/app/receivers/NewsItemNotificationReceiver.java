package com.pdm.thothviewer.app.receivers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.pdm.thothviewer.app.R;
import com.pdm.thothviewer.app.activities.WorkItemsActivity;
import com.pdm.thothviewer.app.providers.ThothContracts;

/**
 * Created by André Jonas on 12/05/2014.
 */
public class NewsItemNotificationReceiver extends BroadcastReceiver {
    private static final String TAG = NewsItemNotificationReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive()");

        long classId = intent.getLongExtra(ThothContracts.EXTRA_CLASS_ID_LONG, 0);
        String courseName = intent.getStringExtra(ThothContracts.EXTRA_COURSE_NAME_STRING);
        int newNewsitemsCount = intent.getIntExtra(ThothContracts.EXTRA_COUNT_INT, 0);

        Notification.Builder builder = new Notification.Builder(context)
                .setSmallIcon(R.drawable.ic_action_download)
                .setContentTitle(context.getString(R.string.new_newsitems))
                .setContentText(courseName + " " +
                        context.getString(R.string.has_new_newsitems))
                .setNumber(newNewsitemsCount)
                // Cancela automaticamente a notificação assim que o utilizador clica nela.
                .setAutoCancel(true);

        /*
        Intent resultIntent = new Intent(Intent.ACTION_VIEW);
        resultIntent.setData(Uri.parse("http://"));
                // Valor necessário à Activity
        PendingIntent pendingIntent = PendingIntent.getActivity(
                context,
                0,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT
        );
        builder.setContentIntent(pendingIntent);
        */
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify((int)classId, builder.getNotification());
    }
}
