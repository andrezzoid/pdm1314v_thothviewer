package com.pdm.thothviewer.app;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.util.Log;

import com.pdm.thothviewer.app.utils.StringUtils;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;

/**
 * Created by André Jonas on 24-03-2014.
 *
 * Loader que faz pedidos HTTP GET e faz parsing da resposta JSON para objetos de domínio.
 * Deve ser disponibilizado ao Loader, aquando a sua criação, um IParser com uma
 * implementação concreta do método parse(String) que conhece a construção exata do JSON
 * contido na resposta.
 */
public class HttpLoader<T> extends AsyncTaskLoader<T> {

    public static final String TAG = "HttpLoader";

    private IParser<T> jsonParser;
    private String url;
    private T data;

    /**
     * Stores away the application context associated with context.
     * Since Loaders can be used across multiple activities it's dangerous to
     * store the context directly; always use {@link #getContext()} to retrieve
     * the Loader's Context, don't use the constructor argument directly.
     * The Context returned by {@link #getContext} is safe to use across
     * Activity instances.
     *
     * @param context used to retrieve the application context.
     */
    public HttpLoader(Context context, String url, IParser<T> jsonParser) {
        super(context);
        this.url = url;
        this.jsonParser = jsonParser;
        this.data = null;
    }

    @Override
    protected boolean onCancelLoad() {
        Log.d(TAG, "onCancelLoad()");
        return false;
    }

    /**
     * Called on a worker thread to perform the actual load and to return
     * the result of the load operation.
     * <p/>
     * Implementations should not deliver the result directly, but should return them
     * from this method, which will eventually end up calling {@link #deliverResult} on
     * the UI thread.  If implementations need to process the results on the UI thread
     * they may override {@link #deliverResult} and do so there.
     * <p/>
     * To support cancellation, this method should periodically check the value of
     * {@link #isLoadInBackgroundCanceled} and terminate when it returns true.
     * Subclasses may also override {@link #cancelLoadInBackground} to interrupt the load
     * directly instead of polling {@link #isLoadInBackgroundCanceled}.
     * <p/>
     * When the load is canceled, this method may either return normally or throw
     * {@link *OperationCanceledException}.  In either case, the {@link android.content.Loader} will
     * call {@link #onCanceled} to perform post-cancellation cleanup and to dispose of the
     * result object, if any.
     *
     * @return The result of the load operation.
     * @throws *OperationCanceledException if the load is canceled during execution.
     * @see #isLoadInBackgroundCanceled
     * @see #cancelLoadInBackground
     * @see #onCanceled
     */
    @Override
    public T loadInBackground() {
        Log.d(TAG, "loadInBackground()");
        T result = null;
        try {
            Log.d(TAG, "GET " + this.url);
            HttpClient httpClient = new DefaultHttpClient();
            HttpGet get = new HttpGet(this.url);
            HttpResponse resp = httpClient.execute(get);
            int statusCode = resp.getStatusLine().getStatusCode();
            if(statusCode == HttpStatus.SC_OK){
                // 200 OK
                String json = StringUtils.getStringFrom(resp.getEntity().getContent());
                result = jsonParser.parse(json);
            }
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        }

        return result;
    }

    /**
     * Sends the result of the load to the registered listener. Should only be called by subclasses.
     * <p/>
     * Must be called from the process's main thread.
     *
     * @param data the result of the load
     */
    @Override
    public void deliverResult(T data) {
        Log.d(TAG, "deliverResult()");
        this.data = data;
        super.deliverResult(data);
    }

    /**
     * Subclasses must implement this to take care of stopping their loader,
     * as per {@link #stopLoading()}.  This is not called by clients directly,
     * but as a result of a call to {@link #stopLoading()}.
     * This will always be called from the process's main thread.
     */
    @Override
    protected void onStopLoading() {
        Log.d(TAG, "onStopLoading()");
        super.onStopLoading();
    }

    /**
     * Subclasses must implement this to take care of loading their data,
     * as per {@link #startLoading()}.  This is not called by clients directly,
     * but as a result of a call to {@link #startLoading()}.
     */
    @Override
    protected void onStartLoading() {
        Log.d(TAG, "onStartLoading()");
        super.onStartLoading();             // FIXME do we really need this?
        if(this.data != null){
            deliverResult(this.data);
        }else{
            forceLoad();
        }
    }
}
