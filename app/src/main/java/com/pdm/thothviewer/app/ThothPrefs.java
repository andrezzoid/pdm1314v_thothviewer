package com.pdm.thothviewer.app;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by André Jonas on 08/05/2014.
 *
 * Singleton Utilitário com semântica para obter e persistir as SharedPreferences da aplicação.
 */
public class ThothPrefs {

    public static final String NAME = "thothviewerdetails";
    public static final String PREF_CURRENT_SEMESTER_STRING = "currentSemester";
    public static final String PREF_CLASSES_SET = "classes";
    public static final String PREF_LAST_UPDATE = "lastUpdate";

    private static ThothPrefs instance;

    private Context context;
    private SharedPreferences prefs;
    private final SharedPreferences.Editor editor;

    private ThothPrefs(Context ctx){
        context = ctx;
        prefs = ctx.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        editor = prefs.edit();
    }

    public static ThothPrefs getInstance(Context ctx){
        if(instance == null){
            instance = new ThothPrefs(ctx);
        }
        return instance;
    }

    public List<String> getClassesList(){
        List<String> classesList = new ArrayList<String>(
                prefs.getStringSet(PREF_CLASSES_SET, new HashSet<String>())
        );
        return classesList != null ? classesList: null;
    }

    public String getSemesterShortName(){
        return prefs.getString(PREF_CURRENT_SEMESTER_STRING, null);
    }

    public void setLastUpdate(long timestamp){
        editor.putLong(PREF_LAST_UPDATE, timestamp);
        editor.apply();
    }

    public long getLastUpdate(){
        return prefs.getLong(PREF_LAST_UPDATE, 0);
    }

    // TODO Métodos de set
    // TODO no caso de setClasses, como é uma operação relativamente demorada -> worker thread
    // TODO pode-se, por exemplo, armazenar na instância os valores das preferências, evitando voltar a ler das SharedPreferences
    // TODO tudo isto pode ser estático ?
}
