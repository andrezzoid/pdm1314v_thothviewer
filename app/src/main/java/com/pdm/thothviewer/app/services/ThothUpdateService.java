package com.pdm.thothviewer.app.services;

import android.app.IntentService;
import android.content.ContentProviderOperation;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.os.RemoteException;
import android.os.SystemClock;
import android.util.Log;

import com.pdm.thothviewer.app.ThothAPI;
import com.pdm.thothviewer.app.ThothPrefs;
import com.pdm.thothviewer.app.providers.ThothContracts;
import com.pdm.thothviewer.app.utils.NetworkUtils;
import com.pdm.thothviewer.app.utils.StringUtils;
import com.pdm.thothviewer.app.utils.ThreadUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by André Jonas on 21-04-2014.
 */
public class ThothUpdateService extends IntentService {

    private static final String TAG = ThothUpdateService.class.getSimpleName();

    private static final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    private static ContentProviderOperation jsonWorkItemToOperation(JSONObject obj, long clazzId) throws JSONException, ParseException {
        return ContentProviderOperation.newInsert(ThothContracts.WorkItems.CONTENT_URI)
                .withValue(ThothContracts.WorkItems._ID, obj.getLong(ThothAPI.GET.WorkItems.ID))
                .withValue(ThothContracts.WorkItems.ACRONYM, obj.getString(ThothAPI.GET.WorkItems.ACRONYM))
                .withValue(ThothContracts.WorkItems.TITLE, obj.getString(ThothAPI.GET.WorkItems.TITLE))
                .withValue(ThothContracts.WorkItems.REQUIRES_SUBMISSION, obj.getBoolean(ThothAPI.GET.WorkItems.REQUIRES_SUBMISSION))
                .withValue(ThothContracts.WorkItems.START_DATE, df.parse(obj.getString(ThothAPI.GET.WorkItems.START_DATE)).getTime())
                .withValue(ThothContracts.WorkItems.DUE_DATE, df.parse(obj.getString(ThothAPI.GET.WorkItems.DUE_DATE)).getTime())
                .withValue(ThothContracts.WorkItems.CLASS_ID, clazzId)
                        // The flip side of using batched operations is that a large batch may lock up
                        // the database for a long time preventing other applications from accessing
                        // data and potentially causing ANRs ("Application Not Responding" dialogs).
                        // To avoid such lockups of the database, make sure to insert "yield points" in
                        // the batch. A yield point indicates to the content provider that before
                        // executing the next operation it can commit the changes that have already
                        // been made, yield to other requests, open another transaction and continue
                        // processing operations. A yield point will not automatically commit the
                        // transaction, but only if there is another request waiting on the database.
                        // Normally a sync adapter should insert a yield point at the beginning of each
                        // raw contact operation sequence in the batch.
                .withYieldAllowed(true)
                .build();
    }

    private static List<Long> workItemsCursorToList(Cursor c){
        ArrayList<Long> list = new ArrayList<Long>();
        int idIdx = c.getColumnIndex(ThothContracts.WorkItems._ID);
        while(c.moveToNext()){
            list.add(c.getLong(idIdx));
        }
        return list;
    }


    public ThothUpdateService() {
        // Nome da worker thread
        super(TAG + "WorkerThread");
    }

    /**
     * This method is invoked on the worker thread with a request to process.
     * Only one Intent is processed at a time, but the processing happens on a
     * worker thread that runs independently from other application logic.
     * So, if this code takes a long time, it will hold up other requests to
     * the same IntentService, but it will not hold up anything else.
     * When all requests have been handled, the IntentService stops itself,
     * so you should not call {@link #stopSelf}.
     *
     * @param intent The value passed to {@link
     *               android.content.Context#startService(android.content.Intent)}.
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        // este é o único método sobre o qual não é necessário chamar o super.
        Log.d(TAG, "onHandleIntent()");
        ThreadUtils.logThreadInfo(TAG);

        // Lista de operações a aplicar em Batch.
        ArrayList<ContentProviderOperation> ops =
                new ArrayList<ContentProviderOperation>();

        // Obtém todas as classes
        Cursor classesCursor = getContentResolver().query(
                ThothContracts.Classes.CONTENT_URI,
                new String[]{
                        ThothContracts.Classes._ID,
                        ThothContracts.Classes.COURSE_UNIT_SHORT_NAME
                },
                null,
                null,
                null
        );
        int idIdx = classesCursor.getColumnIndex(ThothContracts.Classes._ID);
        int courseNameIdx = classesCursor.getColumnIndex(ThothContracts.Classes.COURSE_UNIT_SHORT_NAME);

        while(classesCursor.moveToNext()){
            // FIXME é muito pesado verificar constantemente a conectividade?
            try {
                long classId = classesCursor.getLong(idIdx);
                String courseName = classesCursor.getString(courseNameIdx);

                // Obtém todos os workitems
                Cursor workitemsCursor = getContentResolver().query(
                        ThothContracts.WorkItems.withClassId(classId),
                        new String[]{ThothContracts.WorkItems._ID},
                        null,
                        null,
                        null
                );
                List<Long> workitemsIds = workItemsCursorToList(workitemsCursor);
                // Contagem dos novos workitems
                int newWorkitemsCount = 0;

                // Pedido por todos os Workitems da classe
                String workItemJson =
                        NetworkUtils.httpGet(String.format(ThothAPI.GET.WorkItems.URL, classId));
                JSONObject workItemsRoot = new JSONObject(workItemJson);
                JSONArray jsonWorkItems =
                        workItemsRoot.getJSONArray(ThothAPI.GET.WorkItems.WORKITEMS);
                for (int i = 0; i < jsonWorkItems.length(); i++) {
                    JSONObject workItem = jsonWorkItems.getJSONObject(i);
                    long workitemId = workItem.getLong(ThothAPI.GET.WorkItems.ID);

                    if(!workitemsIds.contains(workitemId)) {
                        // Novo workitem, incrementa e adiciona a operação
                        ++newWorkitemsCount;
                        // Adiciona às operações a aplicar mais à frente pelo Resolver
                        ops.add(jsonWorkItemToOperation(workItem, classId));
                    }
                }

                if(newWorkitemsCount > 0) {
                    // Notifica a existência de novos workitems
                    Intent workitemsIntent = new Intent(ThothContracts.INTENT_ACTION_NEW_WORKITEMS);
                    workitemsIntent.putExtra(ThothContracts.EXTRA_CLASS_ID_LONG, classId);
                    workitemsIntent.putExtra(ThothContracts.EXTRA_COURSE_NAME_STRING, courseName);
                    workitemsIntent.putExtra(
                            ThothContracts.EXTRA_COUNT_INT, newWorkitemsCount);
                    sendBroadcast(workitemsIntent);
                }

                // TODO update newsItems


            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        try {
            getContentResolver().applyBatch(ThothContracts.AUTHORITY, ops);

            // Aqui utiliza-se SystemClock.elapsedRealtime() para manter a integridade
            // com o mecanismo utilizado para indicar a cadência de repetição do
            // alarme que dispara o Serviço.
            ThothPrefs prefs = ThothPrefs.getInstance(this);
            prefs.setLastUpdate(SystemClock.elapsedRealtime());

        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate()");
        // debug
        super.onCreate();
    }

    @Override
    public void onStart(Intent intent, int startId) {
        Log.d(TAG, "onStart()");
        // debug
        super.onStart(intent, startId);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand()");
        // debug
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy()");
        // debug
        super.onDestroy();
    }
}
