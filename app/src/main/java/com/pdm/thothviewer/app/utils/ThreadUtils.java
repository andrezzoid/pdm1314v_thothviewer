package com.pdm.thothviewer.app.utils;

import android.util.Log;

/**
 * Created by André Jonas on 17-04-2014.
 */
public class ThreadUtils {

    private static final String TAG = ThreadUtils.class.getSimpleName();

    public static String getThreadInfo(){
        Thread t = Thread.currentThread();
        long id = t.getId();
        String name = t.getName();
        long priority = t.getPriority();
        String group = t.getThreadGroup().getName();
        String hash = Integer.toHexString(t.hashCode());
        return String.format("THREAD_INFO[hash=%s; name=%s; id=%d; priority=%d; group=%s]", hash, name, id, priority, group);
    }

    public static void logThreadInfo(String tag, String msg){
        Log.d(tag, msg + "\n" + getThreadInfo());
    }

    public static void logThreadInfo(String tag){
        Log.d(tag, getThreadInfo());
    }

    public static void logThreadInfo(){
        Log.d(TAG, getThreadInfo());
    }

    public static void sleepInSecs(int secs){
        try {
            Thread.sleep(secs*1000);
        } catch (InterruptedException e) {
            // don't care
        }
    }

    public static void sleepInSecsUninterruptibly(int secs){
        long remaining = secs*1000;
        long end = System.currentTimeMillis() + remaining;
        boolean interrupted = false;
        try {
            do {
                try {
                    Thread.sleep(remaining);
                    return;
                } catch (InterruptedException e) {
                    interrupted = true;
                    long now = System.currentTimeMillis();
                    remaining = end - now;
                }
            } while (true);
        }finally {
            if (interrupted) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
