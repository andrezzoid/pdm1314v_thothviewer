package com.pdm.thothviewer.app.models;

import android.util.Log;

import com.pdm.thothviewer.app.IParser;
import com.pdm.thothviewer.app.ThothAPI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by André Jonas on 29-03-2014.
 */
public class WorkItemParser implements IParser<List<WorkItem>> {

    private static final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");    // e.g. 2011-10-02T17:00:00
    private static final String TAG = "WorkItemParser";

    @Override
    public List<WorkItem> parse(String json) throws ParseException {
        try {
            JSONObject jsonRoot = new JSONObject(json);
            JSONArray jsonWorkItems = jsonRoot.getJSONArray(ThothAPI.GET.WorkItems.WORKITEMS);
            List<WorkItem> workItems = new LinkedList();
            for (int i = 0; i < jsonWorkItems.length(); i++) {
                JSONObject workItem = jsonWorkItems.getJSONObject(i);

                try {
                    workItems.add(new WorkItem(
                            workItem.getLong(ThothAPI.GET.WorkItems.ID),
                            workItem.getString(ThothAPI.GET.WorkItems.ACRONYM),
                            workItem.getString(ThothAPI.GET.WorkItems.TITLE),
                            workItem.getBoolean(ThothAPI.GET.WorkItems.REQUIRES_SUBMISSION),
                            df.parse(workItem.getString(ThothAPI.GET.WorkItems.START_DATE)).getTime(),
                            df.parse(workItem.getString(ThothAPI.GET.WorkItems.DUE_DATE)).getTime()
                    ));
                } catch (java.text.ParseException e) {
                    Log.d(TAG, "Error parsing datetime");
                }
            }
            return workItems;
        }catch(JSONException e){
            Log.e(TAG, "JSONException" + e.getMessage());
            return null;
        }
    }
}
