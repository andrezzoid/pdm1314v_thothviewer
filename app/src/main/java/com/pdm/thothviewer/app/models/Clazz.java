package com.pdm.thothviewer.app.models;

/**
 * Created by André Jonas on 27-03-2014.
 */
public class Clazz implements DomainModel {

    private long id;
    private String fullName;
    private String courseShortName;
    private String semesterShortName;
    private String className;

    public Clazz(long id, String fullName, String courseShortName, String semesterShortName, String className) {
        this.id = id;
        this.fullName = fullName;
        this.courseShortName = courseShortName;
        this.semesterShortName = semesterShortName;
        this.className = className;
    }

    public long getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }

    public String getCourseShortName() {
        return courseShortName;
    }

    public String getSemesterShortName() {
        return semesterShortName;
    }

    public String getClassName() { return className; }
}
