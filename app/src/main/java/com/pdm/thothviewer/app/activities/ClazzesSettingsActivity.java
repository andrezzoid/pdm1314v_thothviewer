package com.pdm.thothviewer.app.activities;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.pdm.thothviewer.app.HttpLoader;
import com.pdm.thothviewer.app.adapters.AbstractArrayAdapter;
import com.pdm.thothviewer.app.R;
import com.pdm.thothviewer.app.ThothAPI;
import com.pdm.thothviewer.app.models.Clazz;
import com.pdm.thothviewer.app.models.ClazzParser;
import com.pdm.thothviewer.app.services.ThothInitService;
import com.pdm.thothviewer.app.utils.NetworkUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by André Jonas on 29-03-2014.
 */
public class ClazzesSettingsActivity extends Activity
        implements LoaderManager.LoaderCallbacks<List<Clazz>>{

    private static final String TAG = "ClazzesSettingsActivity";
    private static final int LOADER_ID = 0;
    private static final String EXTRA_SEMESTER_STRING = "semester";
    private static final String EXTRA_CLAZZES_STRING = "clazzes";

    private ProgressBar progress;
    private ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_clazzes);

        // verificar se existe ligação à net
        if(!NetworkUtils.isOnline(this)){
            Toast.makeText(this, R.string.network_not_available, Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Network not available!");
            // termina explícitamente a Activity e retorna do método onCreate
            finish();
            return;
        }

        // obter semestre corrente
        SharedPreferences prefs = getSharedPreferences(ThothAPI.PREFS.NAME, MODE_PRIVATE);
        String currentSemester = prefs.getString(ThothAPI.PREFS.PREF_CURRENT_SEMESTER_STRING, null);
        if(currentSemester == null){
            Toast.makeText(this, R.string.current_semester_not_chosen, Toast.LENGTH_LONG).show();
            Log.d(TAG, "Current semester not chosen");
            // termina explícitamente a Activity e retorna do método onCreate
            finish();
            return;
        }

        // obter referências para a View
        progress = (ProgressBar) findViewById(R.id.progressBar);
        listView = (ListView) findViewById(R.id.listView);

        // inicialização do Loader
        Bundle args = new Bundle();
        args.putString(EXTRA_SEMESTER_STRING, currentSemester);
        getLoaderManager().initLoader(LOADER_ID, args, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.confirm, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_confirm) {
            // FIXME as SharedPreferences só permitem receber Set<String>
            // a classe HashSet permite receber uma List como parâmetro
            SparseBooleanArray positions = listView.getCheckedItemPositions();
            ListAdapter adapter = listView.getAdapter();
            Set<String> idsSet = new HashSet();
            for(int i = 0; i < positions.size(); i++){
                if(positions.valueAt(i)){
                    String checkedId = Long.toString(adapter.getItemId(positions.keyAt(i)));
                    idsSet.add(checkedId);
                }

            }

            if(!idsSet.isEmpty()){
                SharedPreferences prefs = getSharedPreferences(ThothAPI.PREFS.NAME, MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putStringSet(ThothAPI.PREFS.PREF_CLASSES_SET, idsSet);
                editor.apply(); //apply asynchronously to the file system.
                //editor.commit(); //The usage of the commit() method is discouraged, as it write the changes synchronously to the file system.
                // semestre escolhido e adicionado às preferêcias,

                // Intent explícito para iniciar o serviço responsável por fazer download das
                // disciplinas escolhidas.
                Intent initIntent = new Intent(this, ThothInitService.class);
                startService(initIntent);

                // termina explícitamente a Activity
                finish();
            }else{
                // utilizador não escolheu nenhum semestre e decidiu armar-se em macaco
                Toast.makeText(this, R.string.choose_one, Toast.LENGTH_LONG).show();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    /*============================================================================================
     * LoaderManager.LoaderCallbacks
     =============================================================================================*/

    @Override
    public Loader<List<Clazz>> onCreateLoader(int id, Bundle args) {
        Log.d(TAG, "onCreateLoader()");

        // Processamento prolongado vai começar, mostrar ProgressBar
        progress.setVisibility(View.VISIBLE);
        listView.setVisibility(View.GONE);

        String semesterName = args.getString(EXTRA_SEMESTER_STRING);
        return new HttpLoader<List<Clazz>>(
                this,
                ThothAPI.GET.Classes.URL,
                new ClazzParser(semesterName)
        );
    }

    @Override
    public void onLoadFinished(Loader<List<Clazz>> loader, List<Clazz> data) {
        Log.d(TAG, "onLoadFinished()");

        // Processamento terminou, esconder a ProgressBar
        progress.setVisibility(View.GONE);
        listView.setVisibility(View.VISIBLE);

        if(data == null || data.isEmpty()){
            Log.d(TAG, "data is null");
            Toast.makeText(this, R.string.no_classes_available, Toast.LENGTH_LONG).show();
        }else{
            listView.setAdapter(new AbstractArrayAdapter<Clazz>(
                    this,
                    android.R.layout.simple_list_item_multiple_choice,
                    data
            ) {
                @Override
                public void bindRow(ViewHolder viewHolder, long position, Clazz obj) {
                    TextView txt = viewHolder.get(android.R.id.text1);
                    txt.setText(obj.getCourseShortName() + " (" + obj.getClassName() + ")");
                }
            });

            // fazer set ao semestre previamente escolhido
            SharedPreferences prefs = getSharedPreferences(ThothAPI.PREFS.NAME, MODE_PRIVATE);
            Set<String> prefClazzes = prefs.getStringSet(ThothAPI.PREFS.PREF_CLASSES_SET,
                    new HashSet<String>());
            if(!prefClazzes.isEmpty()){
                // FIXME não é demasiado processamento para a UI Thread?
                int i = 0;
                for(Clazz clazz: data){
                    if(prefClazzes.contains(Long.toString(clazz.getId()))){
                        listView.setItemChecked(i, true);
                    }
                    i++;
                }

            }
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Clazz>> loader) {
        Log.d(TAG, "onLoaderReset()");
        // do nothing
    }


}
