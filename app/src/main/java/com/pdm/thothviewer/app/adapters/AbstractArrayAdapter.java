package com.pdm.thothviewer.app.adapters;

import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.pdm.thothviewer.app.models.DomainModel;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by André Jonas on 16-04-2014.
 */
public abstract class AbstractArrayAdapter<T extends DomainModel> extends ArrayAdapter<T> {

    /**
     * Guarda as Views de cada linha da lista. Deverá existir uma instância desta classe associada
     * à "tag" de cada linha (ver método View#setTag()).
     */
    public class ViewHolder{
        // http://developer.android.com/reference/android/util/SparseArray.html
        private SparseArray<View> views = new SparseArray<View>();
        private View convertView;

        public ViewHolder(View view){ convertView = view; }

        public <T extends View> T get(int resId){
            View v = views.get(resId);
            if(v == null){
                // se o view element não existe no mapa, vai buscar e adiciona em futuros acessos
                // já retorna a referência para o elemento.
                v = convertView.findViewById(resId);
                views.put(resId, v);
            }
            return (T)v;
        }

    }

    private LayoutInflater inflater;
    private Context ctx;
    private int viewLayoutId;
    private List<T> objs;

    public AbstractArrayAdapter(Context context, int resource, T[] objects) {
        this(context, resource, Arrays.asList(objects));
    }

    public AbstractArrayAdapter(Context context, int resource, List<T> objects) {
        super(context, resource, objects);
        init(context, resource, objects);
    }

    private void init(Context context, int resource, List<T> objects){
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ctx = context;
        viewLayoutId = resource;
        objs = objects;

    }

    @Override
    public long getItemId(int position) {
        // A razão pela qual este Adapter só pode ser parameterizado com classes que implementem
        // DomainModel.
        return objs.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = inflater.inflate(viewLayoutId, null);
            convertView.setTag(new ViewHolder(convertView));
        }
        bindRow((ViewHolder) convertView.getTag(), position, objs.get(position));

        return convertView;
    }

    public abstract void bindRow(ViewHolder viewHolder, long position, T obj);
}
