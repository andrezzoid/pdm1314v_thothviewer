package com.pdm.thothviewer.app.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by André Jonas on 26-03-2014.
 */
public class NetworkUtils {

    /**
     * Verifica se existe conectividade, qualquer que seja (e.g. Wi-Fi, 3G, etc.)
     * @param ctx
     * @return
     */
    public static boolean isOnline(Context ctx) {
        ConnectivityManager connMgr = (ConnectivityManager)
                ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    /**
     * Verifica se existe conectividade por Wi-Fi.
     * @param ctx
     * @return
     */
    public static boolean isOnlineWiFi(Context ctx) {
        ConnectivityManager connMgr = (ConnectivityManager)
                ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo =
                connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return networkInfo != null &&           // há ligação
                networkInfo.isConnected();      // está conectado
    }

    public static String httpGet(String to) throws IOException{
        InputStream stream = null;
        try {
            URL url = new URL(to);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            // Starts the query
            conn.connect();
            int statusCode = conn.getResponseCode();
            if (statusCode == HttpURLConnection.HTTP_OK){
                stream = conn.getInputStream();
                return StringUtils.getStringFrom(stream);
            }
            return null;
        }finally {
            if(stream != null)
                stream.close();
        }
    }
}
