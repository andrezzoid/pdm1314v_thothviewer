package com.pdm.thothviewer.app.receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

import com.pdm.thothviewer.app.ThothPrefs;
import com.pdm.thothviewer.app.services.ThothUpdateService;
import com.pdm.thothviewer.app.utils.NetworkUtils;
import com.pdm.thothviewer.app.utils.ThreadUtils;

/**
 * Created by André Jonas on 16-04-2014.
 */
public class ConnectivityReceiver extends BroadcastReceiver {

    private static final String TAG = ConnectivityReceiver.class.getSimpleName();


    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive() started");
        ThreadUtils.logThreadInfo(TAG); // prova que o código executa na main thread

        //
        // Obter o AlarmManager, criar o Intent e o PendingIntent necessários à repetição
        AlarmManager alarmManager =
                (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, ThothUpdateService.class);
        // http://developer.android.com/reference/android/app/PendingIntent.html
        PendingIntent pending = PendingIntent
                .getService(context, 0, i, PendingIntent.FLAG_CANCEL_CURRENT);

        if(NetworkUtils.isOnlineWiFi(context)){
            Log.d(TAG, "Got Wi-Fi");

            ThothPrefs prefs = ThothPrefs.getInstance(context);
            long lastUpdate = prefs.getLastUpdate();

            //
            // Configurações do Alarme
            // http://developer.android.com/training/scheduling/alarms.html#set
            int alarmType = AlarmManager.ELAPSED_REALTIME;  // Só dispara se dispositivo acordado
            long interval = AlarmManager.INTERVAL_DAY;
            long start = SystemClock.elapsedRealtime();     // Time since the system was booted

            if(lastUpdate + interval > start){
                // Ainda não passou tempo suficiente desde a última actualização, sai
                Log.d(TAG, "Not enough elaped time since last update.");

                return;
            }
            Log.d(TAG, "Setting up " + ThothUpdateService.class.getSimpleName());
            // InexactRepeating permite ao Android optimizar o consumo de energia ao aproveitar
            // a proximidade entre alarmes para os retardar e lançar vários ao mesmo tempo.
            alarmManager.setInexactRepeating(alarmType, start, interval, pending);

            // Forma normal de iniciar o Serviço
            //Intent i = new Intent(context, ThothUpdateService.class);
            //context.startService(i);
        }else{
            Log.d(TAG, "No connectivity, stopping " + ThothUpdateService.class.getSimpleName());
            // Cancela quaisquer alarmes correspondentes ao Intent especificado
            alarmManager.cancel(pending);

            // Forma normal de parar o Serviço
            //Intent i = new Intent(context, ThothUpdateService.class);
            //context.stopService(i);
        }

        Log.d(TAG, "onReceive() ended");
    }
}
