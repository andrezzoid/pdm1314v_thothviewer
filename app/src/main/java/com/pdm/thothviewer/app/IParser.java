package com.pdm.thothviewer.app;

/**
 * Created by André Jonas on 25-03-2014.
 *
 * Interface funcional com método parse(String) cuja implementação concreta deverá ter em
 * consideração o JSON recebido como parâmetro.
 */
public interface IParser<T> {
    public T parse(String json) throws ParseException;


    public class ParseException extends RuntimeException{
        public ParseException() { super(); }
        public ParseException (String s) {
            super(s);
        }
    }
}
